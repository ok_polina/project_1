import pandas as pd


class File:
    def __init__(self, filename):
        self.name = filename

    def read(self):
        with open(f'{self.name}.csv') as f:
            return pd.read_csv(f, delimiter=',')

    def save(self, df):
        df.to_csv(f'{self.name}.csv', index=False)
        return