import argparse
from tqdm import tqdm
from datetime import datetime
import pandas as pd

import os

from file import File


def calc_earlier_entries(list_of_time_values, dt=0.3):
    counts = []
    dict_of_time_freq = {}
    i_l = 0
    for i_r in range(0, len(list_of_time_values), 1):
        while (list_of_time_values[i_r] - list_of_time_values[i_l] > dt) and (i_l < i_r):
            i_l += 1
        dict_of_time_freq[list_of_time_values[i_r]] = i_r - i_l
    for i in range(0, len(list_of_time_values), 1):
        counts.append(dict_of_time_freq[list_of_time_values[i]])
    return counts


def calculate_entries(data, dt):
    results_df = pd.DataFrame(columns=['id', 'count'])
    grouped_data = data.groupby(['feature1', 'feature2'])
    for group in tqdm(grouped_data):
        if len(group[1]) == 1:
            results_df['id'] = group[1].id
            results_df['count'] = 0
            continue

        else:
            sorted_data = group[1].sort_values(by='time', ascending=True)
            sorted_data['count'] = calc_earlier_entries(sorted_data.time.values, dt)
            results_df = results_df.append(sorted_data[['id', 'count']])

    return results_df


def analyse_data(input_name, output_name, dt):
    file_input = File(input_name)
    file_output = File(output_name)
    input_data = file_input.read()
    result_df = calculate_entries(input_data, dt)
    file_output.save(result_df.sort_values(by='id', ascending=True))


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-if', '--input_file', type=str, default='incidents')
    parser.add_argument('-of', '--output_file', type=str, default='output')
    parser.add_argument('-dt', '--dt', type=float, default=3)
    args = parser.parse_args()

    time_start = datetime.now()
    for input_csv, output_csv, dt in [(args.input_file, args.output_file, args.dt)]:
        analyse_data(input_csv, output_csv, dt)

    time_finish = datetime.now()
    print(f"Time of execution is {time_finish - time_start}")

